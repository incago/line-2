﻿using UnityEngine;
using System;
using System.Collections.Generic;       
using Random = UnityEngine.Random;      

public class BoardManager : MonoBehaviour
{
    public int columns = 39;
    public int rows = 7;
    public float gridSize = 1.0f;

    public LayerMask playerLayer;
    public LayerMask enemyLayer;
    public LayerMask obstacleLayer;

    public GameObject obstaclePrefab;
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    
    public Player player;
    private Transform boardHolder; 
    public uint[,] obstacleGrid; //의자같이 스테이지 배경에 속하는 장애물
    public uint[,] mobGrid;
    private List<Vector3> gridPositions = new List<Vector3>();
    
    void Start()
    {
        
    }
    /// <summary>
    /// 몹의 움직임에 제한을 주는 장애물을 만들어냅니다.
    /// </summary>
    void SpawnObstacles()
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                if(obstacleGrid[x, y] != 0)
                {
                    GameObject instance = Instantiate(obstaclePrefab, GetGridPosition(x, y), Quaternion.identity) as GameObject;
                }       
            }
        }
    }

    /// <summary>
    /// Player와 Enemy 그리고 중립 몹을 만들어냅니다.
    /// </summary>
    void SpawnMobs()
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                if (mobGrid[x, y] == 1)
                {
                    GameObject instance = Instantiate(playerPrefab, GetGridPosition(x, y), Quaternion.identity) as GameObject;
                    Player playerInstance = instance.GetComponent<Player>();
                    player = playerInstance;
                    playerInstance.InitMovingObject(x, y);
                }
                else if (mobGrid[x, y] == 10)
                {
                    GameObject instance = Instantiate(enemyPrefab, GetGridPosition(x, y), Quaternion.identity) as GameObject;
                    Enemy enemyInstance = instance.GetComponent<Enemy>();
                    enemyInstance.InitMovingObject(x, y);
                }
            }
        }
    }

    void InitialiseList()
    {
        obstacleGrid = new uint[columns, rows];
        mobGrid = new uint[columns, rows];
        gridPositions.Clear();
        
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                gridPositions.Add(new Vector3(x + (gridSize/2.0f) - (float)columns/2.0f, 0f, y + (gridSize/ 2.0f) - (float)rows/ 2.0f));
                obstacleGrid[x, y] = mobGrid[x, y] = 0;
            }
        }
    }

    public Vector3 GetGridPosition(int x, int y)
    {
        return gridPositions[x + y * columns];
    }

    public bool IsGridEmpty(int _x, int _y)
    {
        bool returnValue = true;

        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                if (obstacleGrid[_x, _y] != 0) return false;
                if (mobGrid[_x, _y] != 0) return false;
            }
        }

        return returnValue;
    }

    public bool IsGridSomething(int _x, int _y, LayerMask targetLayer)
    {
        if (_x < 0 || columns - 1 < _x || _y < 0 || rows - 1 < _y)
        {
            return false;
        }
        bool returnValue = false;
        Vector3 start = GetGridPosition(_x, _y);
        start = new Vector3(start.x, 10.0f, start.z);
        Vector3 fwd = Vector3.down;
        RaycastHit hit;
        
        if(returnValue = Physics.Raycast(start, fwd, out hit, 10.0f, targetLayer, QueryTriggerInteraction.Collide))
        {
            //Debug.Log(hit.collider.name);
        }
        
        return returnValue;
    }

    public uint GetMobGrid(int x, int y)
    {
        return mobGrid[x, y];
    }

    public void SetMobGrid(int x, int y, uint value)
    {
        mobGrid[x, y] = value;
    }

    public void SetupScene(int level)
    {
        InitialiseList();
        
        obstacleGrid[0, 0] = obstacleGrid[1, 0] = obstacleGrid[2, 0] = obstacleGrid[6, 0] = obstacleGrid[7, 0] = obstacleGrid[8, 0] = 2; //chair???
        obstacleGrid[5, 0] = obstacleGrid[5, 2] = obstacleGrid[5, 3] = obstacleGrid[5, 4] = 2;
        obstacleGrid[7, 2] = obstacleGrid[7, 3] = obstacleGrid[7, 4] = obstacleGrid[7, 6] = 2;
        
        mobGrid[3, 5] = 1; //player???

        mobGrid[3, 1] = 10; //enemy
        
        mobGrid[15, 1] = 10; //enemy???
        mobGrid[15, 2] = 10; //enemy???
        mobGrid[15, 3] = 10; //enemy???
        mobGrid[15, 4] = 10; //enemy???
        
        SpawnObstacles();
        SpawnMobs();
    }
    
    public void ProcessAttack(int xGrid, int yGrid, int damage, GameObject attacker)
    {
        if (xGrid < 0 || columns - 1 < xGrid || yGrid < 0 || rows - 1 < yGrid) { return; }
        Vector3 start = GetGridPosition(xGrid, yGrid);
        start = new Vector3(start.x, 10.0f, start.z);
        Vector3 fwd = Vector3.down;
        RaycastHit hit;

        if (Physics.Raycast(start, fwd, out hit, 10.0f))
        {
            ITakeDamage victim = hit.collider.GetComponent<ITakeDamage>();
            if(victim != null)
            {
                victim.TakeDamage(damage, attacker);
            }
        }
        else
        {
            Debug.Log("No attack target founded");
        }   
    }
}
