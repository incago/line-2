﻿using UnityEngine;
using System.Collections;

public abstract class LivingEntity : MonoBehaviour
{
    public int xGrid, yGrid;
    public int attackX, attackY;
    public float moveTime = 0.2f;
    public float moveDelay = 0.1f;
    public float attackTime = 0.5f;
    public LayerMask blockingLayer;
    public bool isMoving = false;
    public bool isAttacking = false;
    public Transform body;

    [Header("Stats")]
    public int maxHp;
    public int hp;
    public int maxAp;
    public int ap;
    public int damage;

    protected Animator animator;
    protected BoardManager boardScript;
    private BoxCollider boxCollider;
    private Rigidbody rb;
    private float inverseMoveTime;

    public void InitMovingObject(int _xGrid, int _yGrid)
    {
        xGrid = _xGrid;
        yGrid = _yGrid;
    }

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        boardScript = GameObject.FindGameObjectWithTag("BoardManager").GetComponent<BoardManager>();
        boxCollider = GetComponent<BoxCollider>();
        rb = GetComponent<Rigidbody>();
        inverseMoveTime = 1f / moveTime;
        hp = maxHp;
    }
    
    protected IEnumerator MoveWrapper(Vector3 end)
    {
        animator.SetFloat("speed", 1.0f);
        isMoving = true;
        yield return StartCoroutine(SmoothMovement(end));
        animator.SetFloat("speed", 0.0f);
        isMoving = false;
    }

    protected IEnumerator SmoothMovement(Vector3 end)
    {
        Debug.Log("Start Position");
        Debug.Log(transform.position);
        Debug.Log("End Position");
        Debug.Log(end);
        Vector3 start = boardScript.GetGridPosition(xGrid, yGrid);
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPostion = Vector3.MoveTowards(transform.position, end, inverseMoveTime * Time.deltaTime);
            rb.MovePosition(newPostion);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;

            yield return null;
        }
    }

    public void AttackInAnimation()
    {
        boardScript.ProcessAttack(attackX, attackY, damage, gameObject);
    }
}
