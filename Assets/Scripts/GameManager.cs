﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;              
    public BoardManager boardScript;
    public float levelStartDelay = 2f;
    public float turnDelay = 0.1f;
    [HideInInspector]
    public bool playersTurn = true;

    private UIManager uiManager;
    private int level = 3;
    public List<Enemy> enemies;
    private bool enemiesMoving;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);
        enemies = new List<Enemy>();
        boardScript = GetComponent<BoardManager>();
        
        InitGame();
    }

    void Update()
    {
        if (playersTurn || enemiesMoving) return;
                
        ActEnemiesCo();
    }

    void InitGame()
    {
        uiManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        boardScript.SetupScene(level);
    }

    public void GameOver()
    {
        enabled = false;
    }

    public void AddEnemyToList(Enemy script)
    {
        enemies.Add(script);
    }

    public void RemoveEnemyFromList(Enemy script)
    {
        enemies.Remove(script);
        if(enemies.Count == 0)
        {
            uiManager.SetTurnButton(TurnState.Clear);
            playersTurn = true;
        }
    }
    
    /// <summary>
    /// 적 턴이 되면 enemies리스트에 등록된 적을 순차적으로 발동시킵니다.(움직임, 공격)
    /// </summary>
    public void ActEnemiesCo()
    {
        StartCoroutine(ActEnemies());
    }

    IEnumerator ActEnemies()
    {
        enemiesMoving = true;
                
        yield return new WaitForSeconds(turnDelay);
        
        if (enemies.Count == 0)
        { yield return new WaitForSeconds(turnDelay); }
        
        for (int i = 0; i < enemies.Count; i++)
        {
            while(enemies[i].ap > 0)
            {
                yield return StartCoroutine(enemies[i].ActEnemy());
            }
            Debug.Log("enemy(" + i.ToString() + ") act done");
        }
        
        playersTurn = true;
        enemiesMoving = false;
        
        uiManager.SetTurnButton(TurnState.Player);
    }

    public void EndPlayerTurn()
    {
        uiManager.SetTurnButton(TurnState.Enemy);
        boardScript.player.ap = boardScript.player.maxAp;
        foreach(Enemy enemy in  enemies)
        {
            enemy.ap = enemy.maxAp;
        }
        playersTurn = false;
    }
}