﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MobHud : MonoBehaviour
{
    Camera camera;

    private LivingEntity target;
    public Vector3 worldOffset;
    public Vector3 screenOffset;

    public GameObject dotPrefab;
    public Transform dotWrapper;
    public Image[] heartImages;

    public Color emptyColor, fillColor;
    
    void Awake()
    {
        camera = Camera.main;
    }

    public void InitHungryHud(LivingEntity mob)
    {
        target = mob;
        heartImages = new Image[target.maxHp];
        for (int i = 0; i < heartImages.Length; i++)
        {
            GameObject dotInstance = Instantiate(dotPrefab) as GameObject;
            dotInstance.transform.SetParent(dotWrapper);
            dotInstance.transform.localScale = Vector3.one;
            heartImages[i] = dotInstance.GetComponentInChildren<Image>();
            heartImages[i].color = fillColor;
        }
    }

    void Update()
    {
        Vector3 screenPoint = camera.WorldToScreenPoint(target.transform.position + worldOffset);
        transform.position = screenPoint + screenOffset;
    }

    public void UpdateMobHud()
    {
        for (int i = target.maxHp - 1; target.hp - 1 < i; i--)
        {
            heartImages[i].color = emptyColor;
        }
    }
}
