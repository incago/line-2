﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Player : LivingEntity, ITakeDamage
{
    public List<Range> moveRange;
    public List<Range> attackRange;

    private UIManager uiManager;

    protected override void Start()
    {
        uiManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        attackRange = new List<Range>();
        attackRange.Add(new Range(1, 0));
        attackRange.Add(new Range(-1, 0));
        attackRange.Add(new Range(0, 1));
        attackRange.Add(new Range(0, -1));
                
        base.Start();
    }
    
    private void Update()
    {
        if((GameManager.instance.playersTurn && ap > 0) || GameManager.instance.enemies.Count == 0  )
            GetInput();
    }

    private void GetInput()
    {
        int horizontal = 0;     
        int vertical = 0;       
                
        horizontal = (int)(Input.GetAxisRaw("Horizontal"));
        vertical = (int)(Input.GetAxisRaw("Vertical"));
        
        if (horizontal != 0) { vertical = 0; }
        
        if (horizontal != 0 || vertical != 0)
        {
            if(!isMoving && !isAttacking) ActPlayer(horizontal, vertical);
        }
    }


    protected void ActPlayer(int xDir, int yDir)
    {
        int targetX = xGrid + xDir;
        int targetY = yGrid + yDir;

        if (targetX < 0 || boardScript.columns - 1 < targetX) return;
        else if (targetY < 0 || boardScript.rows - 1 < targetY) return;

        if (boardScript.IsGridSomething(targetX, targetY, boardScript.enemyLayer))
        {
            body.forward = new Vector3(xDir, 0, yDir);
            SpendAp();
            StartCoroutine(AttackWrapper(targetX, targetY));
        }
        else if (boardScript.IsGridEmpty(targetX, targetY))
        {
            SpendAp();
            Move(xDir, yDir);
        }
    }

    protected void Move(int xDir, int yDir)
    {
        int targetX = xGrid + xDir;
        int targetY = yGrid + yDir;
        Vector3 end = boardScript.GetGridPosition(targetX, targetY);
        uint mobValue = boardScript.GetMobGrid(xGrid, yGrid);
        boardScript.SetMobGrid(xGrid, yGrid, 0);
        xGrid = targetX;
        yGrid = targetY;
        boardScript.SetMobGrid(xGrid, yGrid, mobValue);
        body.forward = new Vector3(xDir, 0, yDir);
        StartCoroutine(MoveWrapper(end));

    }

    protected IEnumerator AttackWrapper(int x, int y)
    {
        isAttacking = true;
        attackX = x;
        attackY = y;
        animator.SetTrigger("attack");
                
        yield return new WaitForSeconds(moveDelay);
    }
    
    public void AttackEnd()
    {
        isAttacking = false;
    }
    
    public void TakeDamage(int damage, GameObject instigator)
    {
        hp -= damage;
        if (hp < 0) hp = 0;

        if (hp == 0)
        {
            OnDead();
        }
    }

    void OnDead()
    {
        GameOver();
    }

    void GameOver()
    {
        Debug.Log("GameOver");
    }

    void SpendAp()
    {
        if(GameManager.instance.enemies.Count != 0)
        {
            ap--;
            if (ap == 0) uiManager.SetTurnButton(TurnState.ApOver);
        }
    }

    Range IsEnemyInRange()
    {
        foreach (Range range in attackRange)
        {
            Range targetGrid = new Range(xGrid, yGrid) + range;
            bool isPlayer = boardScript.IsGridSomething(targetGrid.x, targetGrid.y, boardScript.playerLayer);
            if (isPlayer) return range;
        }
        return null;
    }
}
