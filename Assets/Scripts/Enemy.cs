﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[Serializable]
public class Range
{
    public int x, y;
    public Range(int _x, int _y)
    {
        x = _x;
        y = _y;
    }
    public static Range operator +(Range a, Range b)
    {
        return new Range(a.x + b.x, a.y + b.y);
    }
}

public class Enemy : LivingEntity, ITakeDamage
{
    private UIManager uiManager;
    private Pathfinding pathfinder;
    public int playerDamage;
    
    private Transform target;
    private bool skipMove;
    
    public float sight;
    public List<Range> attackRange;
    public float actDelay;
    public float actRate;
    public MobHud mobHud;
    public MobHud mobHudPrefab;

    protected override void Start()
    {
        uiManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        pathfinder = GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinding>();
        GameManager.instance.AddEnemyToList(this);
        animator = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        attackRange = new List<Range>();
        attackRange.Add(new Range(1, 0));
        attackRange.Add(new Range(-1, 0));
        attackRange.Add(new Range(0, 1));
        attackRange.Add(new Range(0, -1));

        base.Start();
    }
        
    public IEnumerator ActEnemy()
    {   
        if(Vector3.Distance(transform.position, target.position) < sight)
        {
            Range range = IsPlayerInRange();
            if(range != null)
            {
                body.forward = new Vector3((float)range.x, 0, (float)range.y);
                //yield return StartCoroutine(AttackWrapper(xGrid + range.x, yGrid + range.y));
                SpendAp();
                yield return StartCoroutine(Attack(xGrid + range.x, yGrid + range.y));
            }
            else
            {
                Node node = pathfinder.FindPath(transform.position, target.position, xGrid, yGrid);
                if(node != null)
                {
                    SpendAp();
                    yield return StartCoroutine(Move(node.gridX - xGrid, node.gridY - yGrid));
                }
                else
                {
                    ap = 0;
                }
            }
        }
        else
        {
            ap = 0;
        }
    }
        
    Range IsPlayerInRange()
    {
        foreach(Range range in attackRange)
        {
            Range targetGrid = new Range(xGrid, yGrid) + range;
            bool isPlayer = boardScript.IsGridSomething(targetGrid.x, targetGrid.y, boardScript.playerLayer);
            if (isPlayer) return range;
        }
        return null;
    }
    
    public IEnumerator Move(int xDir, int yDir)
    {
        int targetX = xGrid + xDir;
        int targetY = yGrid + yDir;
        
        Vector3 end = boardScript.GetGridPosition(targetX, targetY);
        uint mobValue = boardScript.GetMobGrid(xGrid, yGrid);
        boardScript.SetMobGrid(xGrid, yGrid, 0);
        xGrid = targetX;
        yGrid = targetY;
        boardScript.SetMobGrid(xGrid, yGrid, mobValue);
        body.forward = new Vector3(xDir, 0, yDir);
        yield return  StartCoroutine(MoveWrapper(end));
        //SpendAp();
    }

    public IEnumerator Attack(int _xGrid, int _yGrid)
    {
        yield return StartCoroutine(AttackWrapper(_xGrid, _yGrid));
        
    }

    protected IEnumerator AttackWrapper(int x, int y)
    {
        isAttacking = true;
        attackX = x;
        attackY = y;
        animator.SetTrigger("attack");
        //yield return StartCoroutine(WaitForAnimation("die", 1f, false));
        yield return new WaitForSeconds(attackTime);
    }
    
    public void AttackEnd()
    {
        isAttacking = false;
        Debug.Log("Attack is over");
    }
    
    public void TakeDamage(int damage, GameObject instigator)
    {
        if (!mobHud) MakeMobHud();

        hp -= damage;
        if (hp < 0) hp = 0;
        mobHud.UpdateMobHud();
        if (hp == 0)
        {
            OnDead();
        }
    }

    void OnDead()
    {
        GameManager.instance.RemoveEnemyFromList(gameObject.GetComponent<Enemy>());
        boardScript.SetMobGrid(xGrid, yGrid, 0);
        if (mobHud) Destroy(mobHud.gameObject);
        Destroy(gameObject);
    }

    void SpendAp()
    {
        Debug.Log("SpendAP : " + gameObject.GetInstanceID() + ":"+ Time.realtimeSinceStartup);
        ap--;
    }

    void MakeMobHud()
    {
        MobHud mobHudInstance = Instantiate(mobHudPrefab) as MobHud;
        mobHudInstance.transform.SetParent(uiManager.hudWrapper);
        mobHudInstance.transform.localScale = Vector3.one;
        mobHudInstance.InitHungryHud(this);
        mobHud = mobHudInstance;
    }
}
