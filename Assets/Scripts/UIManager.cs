﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// 현재 턴의 상태를 구분하는 enum입니다.
/// Player : 플레이어턴
/// ApOver : 플레이어턴 + 모든 행동력을 소비
/// Clear : 플레이어턴 + 모든 적이 죽었음
/// Enemy : 적 턴
/// </summary>
public enum TurnState { Player, Enemy, ApOver, Clear }

/// <summary>
/// UI와 관련된 로직입니다.
/// </summary>
public class UIManager : MonoBehaviour {
    public Transform hudWrapper;

    public Button turnButton;
    public Button turnButtonImage;
    public Color playerTurnColor, enemyTurnColor, apOverColor, clearColor;

    /// <summary>
    /// 현재 턴의 상태에 따라 턴 버튼의 색과 문구를 변경합니다.
    /// </summary>
    /// <param name="turnState">현재 턴 상태</param>
    public void SetTurnButton(TurnState turnState)
    {
        Image buttonImage = turnButton.image;
        Text buttonText = turnButton.gameObject.GetComponentInChildren<Text>();
        if (turnState == TurnState.Player)
        {
            buttonText.text = "END TURN";
            buttonImage.color = playerTurnColor;
        }
        else if (turnState == TurnState.Enemy)
        {
            buttonText.text = "ENEMY TURN";
            buttonImage.color = enemyTurnColor;
        }
        else if (turnState == TurnState.ApOver)
        {
            buttonText.text = "END TURN";
            buttonImage.color = apOverColor;
        }
        else if (turnState == TurnState.Clear)
        {
            buttonText.text = "CLEAR";
            buttonImage.color = clearColor;
            turnButton.interactable = false;
        }
    }
}
